-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `webshopdb` DEFAULT CHARACTER SET utf8 ;
USE `webshopdb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webshopdb`.`Customers` (
                                                  `id` INT NOT NULL AUTO_INCREMENT,
                                                  `lastName` VARCHAR(45) NULL,
    `firstName` VARCHAR(45) NULL,
    `phoneNumber` VARCHAR(45) NULL,
    `email` VARCHAR(45) NULL,
    `address` VARCHAR(45) NULL,
    `city` VARCHAR(45) NULL,
    `postalCode` VARCHAR(45) NULL,
    `country` VARCHAR(45) NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Orders`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webshopdb`.`Orders` (
                                               `id` INT NOT NULL AUTO_INCREMENT,
                                               `orderDate` DATE NULL,
                                               `Customers_id` INT NOT NULL,
                                               PRIMARY KEY (`id`),
    INDEX `fk_Orders_Customers1_idx` (`Customers_id` ASC) VISIBLE,
    CONSTRAINT `fk_Orders_Customers1`
    FOREIGN KEY (`Customers_id`)
    REFERENCES `webshopdb`.`Customers` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webshopdb`.`Products` (
                                                 `id` INT NOT NULL AUTO_INCREMENT,
                                                 `productName` VARCHAR(45) NULL,
    `price` DECIMAL(6,2) NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Orderdetails`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webshopdb`.`Orderdetails` (
                                                     `id` INT NOT NULL AUTO_INCREMENT,
                                                     `Products_id` INT NOT NULL,
                                                     `Orders_id` INT NOT NULL,
                                                     `quantity` INT NULL,
                                                     PRIMARY KEY (`id`, `Products_id`, `Orders_id`),
    INDEX `fk_Products_has_Orders_Orders1_idx` (`Orders_id` ASC) VISIBLE,
    INDEX `fk_Products_has_Orders_Products1_idx` (`Products_id` ASC) VISIBLE,
    CONSTRAINT `fk_Products_has_Orders_Products1`
    FOREIGN KEY (`Products_id`)
    REFERENCES `webshopdb`.`Products` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `fk_Products_has_Orders_Orders1`
    FOREIGN KEY (`Orders_id`)
    REFERENCES `webshopdb`.`Orders` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
    ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Admins`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webshopdb`.`Admins` (
                                               `id` INT NOT NULL AUTO_INCREMENT,
                                               `username` VARCHAR(45) NULL,
    `password` VARCHAR(45) NULL,
    PRIMARY KEY (`id`))
    ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
