module at.hakimst.dbapplikation {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires mysql.connector.java;

    opens at.hakimst.dbapplikation.model to javafx.base;
    opens at.hakimst.dbapplikation.controller to javafx.fxml;
    exports at.hakimst.dbapplikation;
}
