package at.hakimst.dbapplikation.controller;


import at.hakimst.dbapplikation.db.CustomerDao;
import at.hakimst.dbapplikation.db.OrderDao;
import at.hakimst.dbapplikation.db.OrderdetailsDao;
import at.hakimst.dbapplikation.db.ProductDao;
import at.hakimst.dbapplikation.model.Customer;
import at.hakimst.dbapplikation.model.Order;
import at.hakimst.dbapplikation.model.Orderdetails;
import at.hakimst.dbapplikation.model.Product;
import at.hakimst.dbapplikation.App;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller der für die (gefilterte) Ausgabe von Datenbankeinträgen zuständig ist
 */
public class ShowAllEntriesController {
    ProductDao productDao;
    OrderDao orderDao;
    OrderdetailsDao orderdetailsDao;
    CustomerDao customerDao;

    @FXML
    private TextField textIdCustomers, textNachnameCustomers, textIdProducts, textBezeichnungProducts, textIdOrder, textKundeOrder, textIdOrderdetails, textBestellungOrderdetails;

    @FXML
    private TableView tableCustomers, tableProducts, tableOrders, tableOrderdetails;

    @FXML
    private TableColumn<Customer, Integer> columnIdCustomer;

    @FXML
    private TableColumn<Customer, String> columnVornameCustomer, columnNachnameCustomer, columnTelNrCustomer, columnEmailCustomer, columnAdresseCustomer, columnOrtCustomer, columnPlzCustomer, columnLandCustomer;

    @FXML
    private TableColumn<Product, Integer> columnIdProducts;

    @FXML
    private TableColumn<Product, String> columnBezeichnungProducts;
    @FXML
    private TableColumn<Product, Double> columnPreisProducts;

    @FXML
    private TableColumn<Order, Integer> columnIdOrder;

    @FXML
    private TableColumn<Order, Date> columnDateOrder;
    @FXML
    private TableColumn<Order, Integer> columnKundeOrder;

    @FXML
    private TableColumn<Orderdetails, Integer> columnIdOrderdetails, columnBestellungOrderdetails, columnProduktOrderdetails, columnMengeOrderdetails;

    public void initialize() {
        customerDao = new CustomerDao();
        productDao = new ProductDao();
        orderDao = new OrderDao();
        orderdetailsDao = new OrderdetailsDao();
        columnIdCustomer.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnVornameCustomer.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnNachnameCustomer.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnTelNrCustomer.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        columnEmailCustomer.setCellValueFactory(new PropertyValueFactory<>("email"));
        columnAdresseCustomer.setCellValueFactory(new PropertyValueFactory<>("address"));
        columnOrtCustomer.setCellValueFactory(new PropertyValueFactory<>("city"));
        columnPlzCustomer.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        columnLandCustomer.setCellValueFactory(new PropertyValueFactory<>("country"));
        columnIdProducts.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnBezeichnungProducts.setCellValueFactory(new PropertyValueFactory<>("productName"));
        columnPreisProducts.setCellValueFactory(new PropertyValueFactory<>("price"));
        columnIdOrder.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnDateOrder.setCellValueFactory(new PropertyValueFactory<>("orderDate"));
        columnKundeOrder.setCellValueFactory(new PropertyValueFactory<>("customers_id"));
        columnIdOrderdetails.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnBestellungOrderdetails.setCellValueFactory(new PropertyValueFactory<>("orders_id"));
        columnProduktOrderdetails.setCellValueFactory(new PropertyValueFactory<>("products_id"));
        columnMengeOrderdetails.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    }

    /**
     * Methode für Button um ins Menü zukrückzukehren
     * @throws IOException
     */
    @FXML
    private void switchToMenu() throws IOException {
        App.setRoot("menu");
    }

    /**
     * Methode um Kunden gefiltert nach ID und/oder Nachname auszugeben
     */
    @FXML
    private void searchCustomer() {
        List<Customer> unfilteredList = customerDao.getAllObjects();
        ArrayList<Customer> filteredList = new ArrayList<>();
        int id;
        if (textIdCustomers.getText().equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(textIdCustomers.getText());
        }
        String nachname = textNachnameCustomers.getText();
        for (Customer c : unfilteredList) {
            if (id != 0 && !nachname.equals("") && id == c.getId() && nachname.equals(c.getLastName())) {
                filteredList.add(c);
            } else if ((id == 0 && !nachname.equals("") && nachname.equals(c.getLastName()))) {
                filteredList.add(c);
            } else if (id != 0 && nachname.equals("") && c.getId() == id) {
                filteredList.add(c);
            }
        }
        tableCustomers.getItems().clear();
        tableCustomers.getItems().addAll(filteredList);
        textIdCustomers.clear();
        textNachnameCustomers.clear();
    }

    /**
     * Methode um alle Kunden auszugeben
     */
    @FXML
    private void showAllCustomers() {
        tableCustomers.getItems().clear();
        tableCustomers.getItems().addAll(customerDao.getAllObjects());
    }

    /**
     * Methode um Produkte gefiltert nach ID und/oder Bezeichnung auszugeben
     */
    @FXML
    private void searchProducts() {
        List<Product> unfilteredList = productDao.getAllObjects();
        ArrayList<Product> filteredList = new ArrayList<>();
        int id;
        if (textIdProducts.getText().equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(textIdProducts.getText());
        }
        String bezeichnung = textBezeichnungProducts.getText();
        for (Product p : unfilteredList) {
            if (id != 0 && !bezeichnung.equals("") && id == p.getId() && bezeichnung.equals(p.getProductName())) {
                filteredList.add(p);
            } else if ((id == 0 && !bezeichnung.equals("") && bezeichnung.equals(p.getProductName()))) {
                filteredList.add(p);
            } else if (id != 0 && bezeichnung.equals("") && p.getId() == id) {
                filteredList.add(p);
            }
        }
        tableProducts.getItems().clear();
        tableProducts.getItems().addAll(filteredList);
        textIdProducts.clear();
        textBezeichnungProducts.clear();
    }

    /**
     * Methode um alle Produkte auszugeben
     */
    @FXML
    private void showAllProducts() {
        tableProducts.getItems().clear();
        tableProducts.getItems().addAll(productDao.getAllObjects());
    }

    /**
     * Methode um Bestellungen gefiltert nach ID und/oder Kunde auszugeben
     */
    @FXML
    private void searchOrders() {
        List<Order> unfilteredList = orderDao.getAllObjects();
        ArrayList<Order> filteredList = new ArrayList<>();
        int id;
        int customer;
        if (textIdOrder.getText().equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(textIdOrder.getText());
        }
        if (textKundeOrder.getText().equals("")) {
            customer = 0;
        } else {
            customer = Integer.parseInt(textKundeOrder.getText());
        }
        for (Order o : unfilteredList) {
            if (id != 0 && customer != 0 && id == o.getId() && customer == o.getCustomers_id()) {
                filteredList.add(o);
            } else if ((id == 0 && customer != 0 && customer == o.getCustomers_id())) {
                filteredList.add(o);
            } else if (id != 0 && customer == 0 && o.getId() == id) {
                filteredList.add(o);
            }
        }
        tableOrders.getItems().clear();
        tableOrders.getItems().addAll(filteredList);
        textIdOrder.clear();
        textKundeOrder.clear();
    }

    /**
     * Methode um alle Bestellungen auszugeben
     */
    @FXML
    private void showAllOrders() {
        tableOrders.getItems().clear();
        tableOrders.getItems().addAll(orderDao.getAllObjects());
    }

    /**
     * Methode um Bestellungsdetails gefiltert nach ID und/oder Bestellung auszugeben
     */
    @FXML
    private void searchOrderDetails() {
        List<Orderdetails> unfilteredList = orderdetailsDao.getAllObjects();
        ArrayList<Orderdetails> filteredList = new ArrayList<>();
        int id;
        int order;
        if (textIdOrderdetails.getText().equals("")) {
            id = 0;
        } else {
            id = Integer.parseInt(textIdOrderdetails.getText());
        }
        if (textBestellungOrderdetails.getText().equals("")) {
            order = 0;
        } else {
            order = Integer.parseInt(textBestellungOrderdetails.getText());
        }
        for (Orderdetails o : unfilteredList) {
            if (id != 0 && order != 0 && id == o.getId() && order == o.getOrders_id()) {
                filteredList.add(o);
            } else if ((id == 0 && order != 0 && order == o.getOrders_id())) {
                filteredList.add(o);
            } else if (id != 0 && order == 0 && o.getId() == id) {
                filteredList.add(o);
            }
        }
        tableOrderdetails.getItems().clear();
        tableOrderdetails.getItems().addAll(filteredList);
        textIdOrderdetails.clear();
        textBestellungOrderdetails.clear();
    }

    /**
     * Methode um alle Bestellungsdetails auszugeben
     */
    @FXML
    private void showAllOrderdetails() {
        tableOrderdetails.getItems().clear();
        tableOrderdetails.getItems().addAll(orderdetailsDao.getAllObjects());
    }
}


