package at.hakimst.dbapplikation.controller;

import at.hakimst.dbapplikation.App;
import at.hakimst.dbapplikation.db.AdminDAO;
import at.hakimst.dbapplikation.model.Admin;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller zur Regelung des Anmeldescreens
 */
public class AdminLoginController {
    private AdminDAO adminDAO;

    @FXML
    private TextField textUsername, textPassword;

    @FXML
    private void initialize(){
        adminDAO = new AdminDAO();
    }

    /**
     * Methode die beim Drücken des Buttons aufgerufen wird um zu prüfen ob der entsprechende Admin existiert
     * @throws IOException
     */
    @FXML
    private void login() throws IOException {
        ArrayList<Admin> admins = (ArrayList<Admin>) adminDAO.getAllObjects();
        for(Admin a : admins){
            if(a.getUsername().equals(textUsername.getText()) && a.getPassword().equals(textPassword.getText())){
                App.setRoot("menu");
            }
        }
        textUsername.clear();
        textPassword.clear();
    }
}
