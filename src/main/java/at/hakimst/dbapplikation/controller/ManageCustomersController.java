package at.hakimst.dbapplikation.controller;

import at.hakimst.dbapplikation.App;
import at.hakimst.dbapplikation.db.CustomerDao;
import at.hakimst.dbapplikation.db.OrderDao;
import at.hakimst.dbapplikation.db.OrderdetailsDao;
import at.hakimst.dbapplikation.db.ProductDao;
import at.hakimst.dbapplikation.model.Customer;
import at.hakimst.dbapplikation.model.Order;
import at.hakimst.dbapplikation.model.Orderdetails;
import at.hakimst.dbapplikation.model.Product;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller um das Hinzufügen, Löschen und Bearbeiten von Kunden zu ermöglichen
 */
public class ManageCustomersController implements IManagement<Customer> {
    private CustomerDao customerDao;


    @FXML
    private TextField lastnameH, firstnameH, telH, mailH, addressH, cityH, zipH, countryH;

    @FXML
    private TextField idB, lastnameB, firstnameB, telB, mailB, addressB, cityB, zipB, countryB;

    @FXML
    private TextField idL;

    @FXML
    private TableView<Customer> tableView;

    @FXML
    private TableColumn<Customer, Integer> columnID;

    @FXML
    private TableColumn<Customer, String> columnFirstName, columnLastName, columnTel, columnMail, columnAddress, columnCity, columnZip, columnCountry;


    @Override
    public void initialize() {
        customerDao = new CustomerDao();
        showAll((ArrayList<Customer>) customerDao.getAllObjects());
    }

    /**
     * Methode für Button um ins Menü zukrückzukehren
     * @throws IOException
     */
    @Override
    public void switchToMenu() throws IOException {
        App.setRoot("menu");
    }

    /**
     * Methode die beim Hinzufügen von Kunden ausgeführt wird
     */
    @Override
    public void add() {
        Customer customer = new Customer(lastnameH.getText(), firstnameH.getText(), telH.getText(), mailH.getText(), addressH.getText(), cityH.getText(), zipH.getText(), countryH.getText());
        customerDao.addObject(customer);
        showAll((ArrayList<Customer>) customerDao.getAllObjects());
        lastnameH.clear();
        firstnameH.clear();
        telH.clear();
        mailH.clear();
        addressH.clear();
        cityH.clear();
        zipH.clear();
        countryH.clear();
    }

    /**
     * Methode die beim Updaten von Kunden ausgeführt wird
     */
    @Override
    public void update() {
        Customer customer = new Customer(Integer.parseInt(idB.getText()), lastnameB.getText(), firstnameB.getText(), telB.getText(), mailB.getText(), addressB.getText(), cityB.getText(), zipB.getText(), countryB.getText());
        customerDao.updateObject(customer);
        showAll((ArrayList<Customer>) customerDao.getAllObjects());
        idB.clear();
        lastnameB.clear();
        firstnameB.clear();
        telB.clear();
        mailB.clear();
        addressB.clear();
        cityB.clear();
        zipB.clear();
        countryB.clear();
    }

    /**
     * Methode die beim Löschen von Kunden ausgeführt wird
     */
    @Override
    public void delete() {
        OrderDao orderDao = new OrderDao();
        int customerID = Integer.parseInt(idL.getText());
        for (Order o : orderDao.getAllObjects()) {
            if (o.getCustomers_id() == customerID) {
                orderDao.deleteObject(o.getId());
            }
        }
        customerDao.deleteObject(customerID);
        showAll((ArrayList<Customer>) customerDao.getAllObjects());
        idL.clear();
    }

    /**
     * Methode die mithilfe einer Arraylist alle Kunden der Datenbank in der Tabelle angezeigt.
     * @param entriesList
     */
    @Override
    public void showAll(ArrayList<Customer> entriesList) {
        columnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnTel.setCellValueFactory(new PropertyValueFactory<>("phoneNumber"));
        columnMail.setCellValueFactory(new PropertyValueFactory<>("email"));
        columnAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        columnCity.setCellValueFactory(new PropertyValueFactory<>("city"));
        columnZip.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
        columnCountry.setCellValueFactory(new PropertyValueFactory<>("country"));
        tableView.getItems().clear();
        tableView.getItems().addAll(entriesList);

    }
}
