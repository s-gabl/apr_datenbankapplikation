package at.hakimst.dbapplikation.controller;

import at.hakimst.dbapplikation.App;
import at.hakimst.dbapplikation.db.CustomerDao;
import at.hakimst.dbapplikation.db.OrderDao;
import at.hakimst.dbapplikation.db.OrderdetailsDao;
import at.hakimst.dbapplikation.model.Customer;
import at.hakimst.dbapplikation.model.Order;
import at.hakimst.dbapplikation.model.Orderdetails;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;

/**
 * Controller um das Hinzufügen, Löschen und Bearbeiten von Bestellungen zu ermöglichen
 */
public class ManageOrdersController implements IManagement {
    private OrderDao orderDao;
    private CustomerDao customerDao;

    @FXML
    private DatePicker dateH, dateB;

    @FXML
    private TextField idB, idL;

    @FXML
    private ChoiceBox<Integer> choiceBoxCustomerH, choiceBoxCustomerB;

    @FXML
    private TableView<Order> tableView;

    @FXML
    private TableColumn<Order, Integer> columnID, columnCustomer;

    @FXML
    private TableColumn<Order, Date> columnOrderdate;

    @Override
    public void initialize() {
        orderDao = new OrderDao();
        customerDao = new CustomerDao();
        createDropdownCustomer();
        showAll((ArrayList) orderDao.getAllObjects());
    }

    /**
     * Methode für Button um ins Menü zukrückzukehren
     * @throws IOException
     */
    @Override
    public void switchToMenu() throws IOException {
        App.setRoot("menu");
        showAll((ArrayList) orderDao.getAllObjects());
    }

    /**
     * Methode die beim Hinzufügen von Bestellungen ausgeführt wird
     */
    @Override
    public void add() {
        orderDao.addObject(new Order(Date.valueOf(dateH.getValue()), choiceBoxCustomerH.getValue()));
        showAll((ArrayList) orderDao.getAllObjects());
    }

    /**
     * Methode die beim Updaten von Bestellungen ausgeführt wird
     */
    @Override
    public void update() {
        orderDao.updateObject(new Order(Integer.parseInt(idB.getText()), Date.valueOf(dateB.getValue()), choiceBoxCustomerB.getValue()));
        idB.clear();
        showAll((ArrayList) orderDao.getAllObjects());
    }

    /**
     * Methode die beim Löschen von Bestellungen ausgeführt wird
     */
    @Override
    public void delete() {
        OrderdetailsDao orderdetailsDao = new OrderdetailsDao();
        int orderID = Integer.parseInt(idL.getText());
        for (Orderdetails o : orderdetailsDao.getAllObjects()) {
            if (o.getOrders_id() == orderID) {
                orderdetailsDao.deleteObject(o.getId());
            }
        }
        orderDao.deleteObject(orderID);
        idL.clear();
        showAll((ArrayList) orderDao.getAllObjects());
    }

    /**
     * Methode die mithilfe einer Arraylist alle Bestellungen der Datenbank in der Tabelle angezeigt.
     * @param entriesList
     */
    @Override
    public void showAll(ArrayList entriesList) {
        columnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnOrderdate.setCellValueFactory(new PropertyValueFactory<>("orderDate"));
        columnCustomer.setCellValueFactory(new PropertyValueFactory<>("customers_id"));
        tableView.getItems().clear();
        tableView.getItems().addAll(entriesList);
    }

    /**
     * Mithilfe dieser Methode wird die Choicebox für die Kunden erstellt
     */
    public void createDropdownCustomer() {
        ArrayList<Customer> list = (ArrayList<Customer>) customerDao.getAllObjects();
        ArrayList<Integer> customerIDlist = new ArrayList<>();
        for (Customer c : list) {
            customerIDlist.add(c.getId());
        }
        choiceBoxCustomerH.getItems().addAll(customerIDlist);
        choiceBoxCustomerB.getItems().addAll(customerIDlist);
    }
}
