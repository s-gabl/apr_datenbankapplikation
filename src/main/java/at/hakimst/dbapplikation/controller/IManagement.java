package at.hakimst.dbapplikation.controller;

import javafx.fxml.FXML;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Interface zum festlegen von Methoden (überwiegend Button-Aktionen) die bei allen Verwaltungscontrollern gebraucht werden.
 * @param <E>
 */
public interface IManagement<E> {
    @FXML
    void initialize();

    @FXML
    void switchToMenu() throws IOException;

    @FXML
    void add();

    @FXML
    void update();

    @FXML
    void delete();

    void showAll(ArrayList<E> entriesList);
}
