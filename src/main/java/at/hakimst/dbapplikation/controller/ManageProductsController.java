package at.hakimst.dbapplikation.controller;

import at.hakimst.dbapplikation.App;
import at.hakimst.dbapplikation.db.OrderDao;
import at.hakimst.dbapplikation.db.OrderdetailsDao;
import at.hakimst.dbapplikation.db.ProductDao;
import at.hakimst.dbapplikation.model.Order;
import at.hakimst.dbapplikation.model.Orderdetails;
import at.hakimst.dbapplikation.model.Product;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller um das Hinzufügen, Löschen und Bearbeiten von Produkten zu ermöglichen
 */
public class ManageProductsController implements IManagement<Product> {
    private ProductDao productDao;

    @FXML
    private TextField textHbezeichnung, textHpreis, textBid, textBbezeichnung, textBpreis, textLid;

    @FXML
    private TableView<Product> tableView;

    @FXML
    private TableColumn<Product, Integer> columnID;

    @FXML
    private TableColumn<Product, String> columnBezeichnung;
    @FXML
    private TableColumn<Product, Double> columnPreis;

    @Override
    public void initialize() {
        productDao = new ProductDao();
        showAll((ArrayList<Product>) productDao.getAllObjects());
    }

    /**
     * Methode für Button um ins Menü zukrückzukehren
     * @throws IOException
     */
    @Override
    public void switchToMenu() throws IOException {
        App.setRoot("menu");
    }

    /**
     * Methode die beim Hinzufügen von Produkten ausgeführt wird
     */
    @Override
    public void add() {
        productDao.addObject(new Product(textHbezeichnung.getText(), Double.parseDouble(textHpreis.getText())));
        showAll((ArrayList<Product>) productDao.getAllObjects());
        textHbezeichnung.clear();
        textHpreis.clear();
    }

    /**
     * Methode die beim Updaten von Produkten ausgeführt wird
     */
    @Override
    public void update() {
        productDao.updateObject(new Product(Integer.parseInt(textBid.getText()), textBbezeichnung.getText(), Double.parseDouble(textBpreis.getText())));
        showAll((ArrayList<Product>) productDao.getAllObjects());
        textBid.clear();
        textBbezeichnung.clear();
        textBpreis.clear();
    }

    /**
     * Methode die beim Löschen von Produkten ausgeführt wird
     */
    @Override
    public void delete() {
        OrderdetailsDao orderdetailsDao = new OrderdetailsDao();
        int productID = Integer.parseInt(textLid.getText());
        for (Orderdetails o : orderdetailsDao.getAllObjects()) {
            if (o.getProducts_id() == productID) {
                orderdetailsDao.deleteObject(o.getId());
            }
        }
        productDao.deleteObject(productID);
        showAll((ArrayList<Product>) productDao.getAllObjects());
        textLid.clear();
    }

    /**
     * Methode die mithilfe einer Arraylist alle Produkte der Datenbank in der Tabelle angezeigt.
     * @param entriesList
     */
    @Override
    public void showAll(ArrayList<Product> entriesList) {
        columnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnBezeichnung.setCellValueFactory(new PropertyValueFactory<>("productName"));
        columnPreis.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableView.getItems().clear();
        tableView.getItems().addAll(entriesList);
    }
}
