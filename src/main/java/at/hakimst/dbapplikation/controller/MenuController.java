package at.hakimst.dbapplikation.controller;

import javafx.fxml.FXML;
import at.hakimst.dbapplikation.App;

import java.io.IOException;

/**
 * Klasse um zu den FXMLs zu kommen, bei denen Bestellungen, Produkte, etc. hinzugefügt bzw. angezeigt werden können
 */
public class MenuController {
    /**
     * Methode um zu FXML für die Ausgabe zu kommen
     * @throws IOException
     */
    @FXML
    private void switchToShowAllEntries() throws IOException {
        App.setRoot("showAllEntries");
    }

    /**
     * Methode um zu FXML zu kommen bei der Produkte verwaltet werden
     * @throws IOException
     */
    @FXML
    private void switchToProducts() throws IOException {
        App.setRoot("manageProducts");
    }

    /**
     * Methode um zu FXML zu kommen bei der Bestellungen verwaltet werden
     * @throws IOException
     */
    @FXML
    private void switchToOrders() throws IOException {
        App.setRoot("manageOrders");
    }

    /**
     * Methode um zu FXML zu kommen bei der Kunden verwaltet werden
     * @throws IOException
     */
    @FXML
    private void switchToCustomers() throws IOException {
        App.setRoot("manageCustomers");
    }

    /**
     * Methode um zu FXML zu kommen bei der Bestellungsdetails verwaltet werden
     * @throws IOException
     */
    @FXML
    private void switchToOrderdetails() throws IOException {
        App.setRoot("manageOrderdetails");
    }
}
