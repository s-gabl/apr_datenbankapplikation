package at.hakimst.dbapplikation.controller;

import at.hakimst.dbapplikation.App;
import at.hakimst.dbapplikation.db.OrderDao;
import at.hakimst.dbapplikation.db.OrderdetailsDao;
import at.hakimst.dbapplikation.db.ProductDao;
import at.hakimst.dbapplikation.model.Order;
import at.hakimst.dbapplikation.model.Orderdetails;
import at.hakimst.dbapplikation.model.Product;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller um das Hinzufügen, Löschen und Bearbeiten von Bestellungsdetails zu ermöglichen
 */
public class ManageOrderdetailsController implements IManagement {
    private OrderdetailsDao orderdetailsDao;
    private OrderDao orderDao;
    private ProductDao productDao;

    @FXML
    private ChoiceBox<Integer> choiceBoxProductH, choiceBoxProductB, choiceBoxOrderH, choiceBoxOrderB;

    @FXML
    private TextField idB, idL, quantityH, quantityB;

    @FXML
    private TableView<Order> tableView;

    @FXML
    private TableColumn<Orderdetails, Integer> columnID, columnOrder, columnProduct, columnQuantity;

    @Override
    public void initialize() {
        orderdetailsDao = new OrderdetailsDao();
        orderDao = new OrderDao();
        productDao = new ProductDao();
        createDropdownProduct();
        createDropdownOrder();
        showAll((ArrayList) orderdetailsDao.getAllObjects());
    }

    /**
     * Methode für Button um ins Menü zukrückzukehren
     * @throws IOException
     */
    @Override
    public void switchToMenu() throws IOException {
        App.setRoot("menu");
    }

    /**
     * Methode die beim Hinzufügen von Bestellungsdetails ausgeführt wird
     */
    @Override
    public void add() {
        orderdetailsDao.addObject(new Orderdetails(choiceBoxProductH.getValue(), choiceBoxOrderH.getValue(), Integer.parseInt(quantityH.getText())));
        quantityH.clear();
        showAll((ArrayList) orderdetailsDao.getAllObjects());
    }

    /**
     * Methode die beim Updaten von Bestellungsdetails ausgeführt wird
     */
    @Override
    public void update() {
        orderdetailsDao.updateObject(new Orderdetails(Integer.parseInt(idB.getText()), choiceBoxProductB.getValue(), choiceBoxOrderB.getValue(), Integer.parseInt(quantityB.getText())));
        idB.clear();
        quantityB.clear();
        showAll((ArrayList) orderdetailsDao.getAllObjects());
    }

    /**
     * Methode die beim Löschen von Bestellungsdetails ausgeführt wird
     */
    @Override
    public void delete() {
        orderdetailsDao.deleteObject(Integer.parseInt(idL.getText()));
        idL.clear();
        showAll((ArrayList) orderdetailsDao.getAllObjects());
    }

    /**
     * Methode die mithilfe einer Arraylist alle Bestellungsdetails der Datenbank in der Tabelle angezeigt.
     * @param entriesList
     */
    @Override
    public void showAll(ArrayList entriesList) {
        columnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        columnOrder.setCellValueFactory(new PropertyValueFactory<>("orders_id"));
        columnProduct.setCellValueFactory(new PropertyValueFactory<>("products_id"));
        columnQuantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tableView.getItems().clear();
        tableView.getItems().addAll(entriesList);
    }

    /**
     * Mithilfe dieser Methode wird die Choicebox für die Produkte erstellt
     */
    public void createDropdownProduct() {
        ArrayList<Product> list = (ArrayList<Product>) productDao.getAllObjects();
        ArrayList<Integer> productIDlist = new ArrayList<>();
        for (Product p : list) {
            productIDlist.add(p.getId());
        }
        choiceBoxProductH.getItems().addAll(productIDlist);
        choiceBoxProductB.getItems().addAll(productIDlist);
    }

    /**
     * Mithilfe dieser Methode wird die Choicebox für die Bestellungen erstellt
     */
    public void createDropdownOrder() {
        ArrayList<Order> list = (ArrayList<Order>) orderDao.getAllObjects();
        ArrayList<Integer> orderIDlist = new ArrayList<>();
        for (Order o : list) {
            orderIDlist.add(o.getId());
        }
        choiceBoxOrderH.getItems().addAll(orderIDlist);
        choiceBoxOrderB.getItems().addAll(orderIDlist);
    }
}
