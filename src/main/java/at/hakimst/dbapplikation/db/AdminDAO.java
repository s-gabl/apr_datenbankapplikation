package at.hakimst.dbapplikation.db;

import at.hakimst.dbapplikation.model.Admin;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementierung des DAOs für Admin
 * Informationen zu den Methoden in IDao.java
 */
public class AdminDAO implements IDao<Admin> {
    private final static String TABLE_NAME = "Admins";

    @Override
    public List<Admin> getAllObjects() {
        List<Admin> adminList = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Admin admin = new Admin(set.getInt(1), set.getString(2), set.getString(3));
                adminList.add(admin);
            }
            return adminList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return adminList;
    }

    @Override
    public Admin getObject(int i) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Admin admin = new Admin(set.getInt(1), set.getString(2), set.getString(3));
            return admin;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(Admin admin) {
        String sql = "INSERT INTO " + TABLE_NAME + " (username, password) VALUES (?, ?)";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, admin.getUsername());
            ps.setString(2, admin.getPassword());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            admin.setId(keys.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(Admin admin) {
        String sql = "UPDATE " + TABLE_NAME + " set username = ?, password = ? WHERE id = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setString(1, admin.getUsername());
            ps.setString(2, admin.getPassword());
            ps.setInt(3, admin.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int i) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(Admin admin) {
        deleteObject(admin.getId());
    }
}
