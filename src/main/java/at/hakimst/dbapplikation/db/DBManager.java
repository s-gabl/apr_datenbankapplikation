package at.hakimst.dbapplikation.db;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Klasse die sich um die Verbindung zur Datenbank kümmert
 */
public class DBManager {
    private static Connection connection;
    private final static String HOST = "127.0.0.1";
    private final static String PORT = "3306";
    private final static String DB_NAME = "webshopdb";
    private final static String USER = "root";
    private final static String PWD = "123";


    private DBManager() {
    }


    /**
     * Methode um eine Connection zu liefern, bzw. eine zu erstellen, falls keine vorhanden sein sollte
     * @return
     */
    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        }
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + HOST + ":" + PORT + "/" + DB_NAME, USER, PWD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

}
