package at.hakimst.dbapplikation.db;

import at.hakimst.dbapplikation.model.Orderdetails;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementierung des DAOs für Bestellungsdetails
 * Informationen zu den Methoden in IDao.java
 */
public class OrderdetailsDao implements IDao<Orderdetails> {
    private static final String TABLE_NAME = "Orderdetails";

    @Override
    public List<Orderdetails> getAllObjects() {
        List<Orderdetails> orderdetailsList = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            while (set.next()) {
                Orderdetails orderdetails = new Orderdetails(set.getInt(1), set.getInt(2), set.getInt(3), set.getInt(4));
                orderdetailsList.add(orderdetails);
            }
            return orderdetailsList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Orderdetails getObject(int i) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ResultSet set = ps.executeQuery();
            set.next();
            Orderdetails orderdetails = new Orderdetails(set.getInt(1), set.getInt(2), set.getInt(3), set.getInt(4));
            return orderdetails;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(Orderdetails orderdetails) {
        String sql = "INSERT INTO " + TABLE_NAME + " (Products_id,Orders_id,quantity) VALUES (?,?,?)";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, orderdetails.getProducts_id());
            ps.setInt(2, orderdetails.getOrders_id());
            ps.setInt(3, orderdetails.getQuantity());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            orderdetails.setId(keys.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(Orderdetails orderdetails) {
        String sql = "UPDATE " + TABLE_NAME + " set Products_id = ?, Orders_id = ?, quantity = ? WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, orderdetails.getProducts_id());
            ps.setInt(2, orderdetails.getOrders_id());
            ps.setInt(3, orderdetails.getQuantity());
            ps.setInt(4, orderdetails.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int i) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(Orderdetails orderdetails) {
        deleteObject(orderdetails.getId());
    }
}
