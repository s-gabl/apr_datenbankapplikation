package at.hakimst.dbapplikation.db;

import java.util.List;

/**
 * Interface für die einzelnen DAOs
 * @param <E>
 */
public interface IDao<E> {
    /**
     * Methode um alle Datensätze in einer Liste zurückzubekommen
     * @return
     */
    List<E> getAllObjects();

    /**
     * Methode um einen Datensatz mithilfe seiner ID als Objekt zurückzubekommen
     * @param i
     * @return
     */
    E getObject(int i);

    /**
     * Hinzufügen eines neuen Datensatzes mithilfe eines Objekts
     * @param e
     */
    void addObject(E e);

    /**
     * Bearbeiten eines bereits bestehenden Datensatzes mithilfe eines Objektes
     * @param e
     */
    void updateObject(E e);

    /**
     * Löschen eines Datensatzes mithilfe von der ID eines Objektes
     * @param i
     */
    void deleteObject(int i);

    /**
     * Löschen eines Datensatzes mithilfe eines Objektes
     * @param e
     */
    void deleteObject(E e);
}
