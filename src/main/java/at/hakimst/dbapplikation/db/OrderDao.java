package at.hakimst.dbapplikation.db;

import at.hakimst.dbapplikation.model.Order;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementierung des DAOs für Bestellungen
 * Informationen zu den Methoden in IDao.java
 */
public class OrderDao implements IDao<Order> {
    private final static String TABLE_NAME = "Orders";

    @Override
    public List<Order> getAllObjects() {
        List<Order> orderList = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Order order = new Order(set.getInt(1), set.getDate(2), set.getInt(3));
                orderList.add(order);
            }
            return orderList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderList;
    }

    @Override
    public Order getObject(int i) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Order order = new Order(set.getInt(1), set.getDate(2), set.getInt(3));
            return order;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(Order order) {
        String sql = "INSERT INTO " + TABLE_NAME + " (orderDate, Customers_id) VALUES (?,?)";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setDate(1, new java.sql.Date(order.getOrderDate().getTime()));
            ps.setInt(2, order.getCustomers_id());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            order.setId(keys.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(Order order) {
        String sql = "UPDATE " + TABLE_NAME + " set orderDate = ?, Customers_id = ? WHERE ID = ?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setDate(1, new java.sql.Date(order.getOrderDate().getTime()));
            ps.setInt(2, order.getCustomers_id());
            ps.setInt(3, order.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int i) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(Order order) {
        deleteObject(order.getId());
    }
}
