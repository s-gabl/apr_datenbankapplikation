package at.hakimst.dbapplikation.db;

import at.hakimst.dbapplikation.model.Product;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementierung des DAOs für Produkte
 * Informationen zu den Methoden in IDao.java
 */
public class ProductDao implements IDao<Product> {
    private static final String TABLE_NAME = "Products";

    @Override
    public List<Product> getAllObjects() {
        List<Product> productList = new ArrayList<>();
        String sql = "SELECT * FROM " + TABLE_NAME;
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            while (set.next()) {
                Product product = new Product(set.getInt(1), set.getString(2), set.getDouble(3));
                productList.add(product);
            }
            return productList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }

    @Override
    public Product getObject(int i) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ResultSet set = ps.executeQuery();
            int numCols = set.getMetaData().getColumnCount();
            set.next();
            Product product = new Product(i, set.getString(2), set.getDouble(3));
            return product;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addObject(Product product) {
        String sql = "INSERT INTO " + TABLE_NAME + " (productName, price) VALUES (?, ?)";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, product.getProductName());
            ps.setDouble(2, product.getPrice());
            ps.executeUpdate();
            ResultSet keys = ps.getGeneratedKeys();
            keys.next();
            product.setId(keys.getInt(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateObject(Product product) {
        String sql = "UPDATE " + TABLE_NAME + " set productName = ?, price = ? WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setString(1, product.getProductName());
            ps.setDouble(2, product.getPrice());
            ps.setInt(3, product.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(int i) {
        String sql = "DELETE FROM " + TABLE_NAME + " WHERE id=?";
        try {
            PreparedStatement ps = DBManager.getConnection().prepareStatement(sql);
            ps.setInt(1, i);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteObject(Product product) {
        deleteObject(product.getId());
    }
}
