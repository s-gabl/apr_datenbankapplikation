package at.hakimst.dbapplikation.model;

import java.sql.Date;

public class Order {
    private int id;
    private Date orderDate;
    private int customers_id;


    public Order(Date orderDate, int customers_id) {
        super();
        this.orderDate = orderDate;
        this.customers_id = customers_id;
    }

    public Order(int id, Date orderDate, int customers_id) {
        super();
        this.id = id;
        this.orderDate = orderDate;
        this.customers_id = customers_id;
    }

    public int getId() {
        return id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public int getCustomers_id() {
        return customers_id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "at.hakimst.dbapplikation.model.Order{id=" + id + ", orderDate=" + orderDate + ", customers_id=" + customers_id + "}";
    }
}
