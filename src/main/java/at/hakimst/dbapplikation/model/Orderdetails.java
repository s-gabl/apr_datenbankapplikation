package at.hakimst.dbapplikation.model;

public class Orderdetails {
    private int id;
    private int products_id;
    private int orders_id;
    private int quantity;

    public Orderdetails(int products_id, int orders_id, int quantity) {
        super();
        this.products_id = products_id;
        this.orders_id = orders_id;
        this.quantity = quantity;
    }

    public Orderdetails(int id, int products_id, int orders_id, int quantity) {
        super();
        this.id = id;
        this.products_id = products_id;
        this.orders_id = orders_id;
        this.quantity = quantity;
    }


    public int getId() {
        return id;
    }

    public int getProducts_id() {
        return products_id;
    }

    public int getOrders_id() {
        return orders_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "at.hakimst.dbapplikation.model.Orderdetails{id=" + id + ", products_id=" + products_id + ", orders_id=" + orders_id + ", quantity=" + quantity + "}";
    }
}
