package at.hakimst.dbapplikation.model;

public class Customer {
    private int id;
    private String lastName;
    private String firstName;
    private String phoneNumber;
    private String email;
    private String address;
    private String city;
    private String postalCode;
    private String country;

    public Customer(String lastName, String firstName, String phoneNumber, String email, String address, String city, String postalCode, String country) {
        super();
        this.lastName = lastName;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
    }

    public Customer(int id, String lastName, String firstName, String phoneNumber, String email, String address, String city, String postalCode, String country) {
        super();
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "at.hakimst.dbapplikation.model.Customer{id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", phoneNumber=" + phoneNumber + ", email=" + email + ", address=" + address + ", city=" + city + ", postalCode=" + postalCode + ", country=" + country + "}";
    }
}
