package at.hakimst.dbapplikation.model;


public class Product {
    private int id;
    private String productName;
    private double price;

    public Product(String productName, double price) {
        super();
        this.productName = productName;
        this.price = price;
    }

    public Product(int id, String productName, double price) {
        super();
        this.id = id;
        this.productName = productName;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public double getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
       // return "at.hakimst.dbapplikation.model.Product{id=" + id + ", productName=" + productName + ", price=" + price + "}";

        return productName + " -- €" + price + " (Art.Nr. "+id+ ");";



    }
}
